﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;

namespace AccountActivities
{
    public class ActivityClosedPlugin: IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // checking plugin registration
            if (context.Stage != 40)
            {
                throw new InvalidPluginExecutionException("Must run as post-op...");
            }
            if (!context.MessageName.Equals("SetState")
                && !context.MessageName.Equals("SetStateDynamicEntity"))
            {
                throw new InvalidPluginExecutionException("Should be registered as SetState/SetStateDynamicEntity not as " + context.MessageName + "!");
            }
            string activityName = context.PrimaryEntityName;
            if (!activityName.Equals("task")
                && !activityName.Equals("appointment")
                && !activityName.Equals("email")
                && !activityName.Equals("phonecall"))
            {
                throw new InvalidPluginExecutionException("Entity of wrong type: " + activityName + "!");
            }

            // init tracing service (trace logs only show if an exception occurs?)
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            tracingService.Trace("In plugin - registration validated.");

            if (context.InputParameters.Contains("EntityMoniker"))
            {
                EntityReference targetEntity = (EntityReference)context.InputParameters["EntityMoniker"];
                OptionSetValue state = (OptionSetValue)context.InputParameters["State"];
                // OptionSetValue status = (OptionSetValue)context.InputParameters["Status"];

                // init org service with system account (null) - could use user id from context
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = serviceFactory.CreateOrganizationService(null);

                tracingService.Trace("Retrieving the activity from db..");
                Entity dbActivity = service.Retrieve(activityName, targetEntity.Id, new ColumnSet(new[] { "regardingobjectid" }));

                if (dbActivity == null)
                {
                    throw new InvalidPluginExecutionException("Activity not found!");
                }
                tracingService.Trace("Activity retrieved..");
                
                if (state.Value == 1)
                {
                    EntityReference regObject = dbActivity.GetAttributeValue<EntityReference>("regardingobjectid");

                    tracingService.Trace("Acc id retrieved from created activity: " + regObject.Id + ", recalculate..");

                    CalculateRollupFieldRequest crfr = new CalculateRollupFieldRequest
                    {
                        Target = new EntityReference("account", regObject.Id),
                        FieldName = "new_next_activity"
                    };
                    service.Execute(crfr);

                    tracingService.Trace("Finished..");
                }
                else
                {
                    throw new InvalidPluginExecutionException("Wrong state code: " + state.Value);
                }
            }
            else
            {
                throw new InvalidPluginExecutionException("wrong context");
            }
        }
    }
}
