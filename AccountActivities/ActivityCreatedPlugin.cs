﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;

namespace AccountActivities
{
    public class ActivityCreatedPlugin : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // checking plugin registration
            if (context.Stage != 40)
            {
                throw new InvalidPluginExecutionException("Must run as post-op...");
            }
            if (!context.MessageName.Equals("Create"))
            {
                throw new InvalidPluginExecutionException("Should be registered as Create not as "+ context.MessageName+"!");
            }
            string activityName = context.PrimaryEntityName;
            if (!activityName.Equals("task")
                && !activityName.Equals("appointment")
                && !activityName.Equals("email")
                && !activityName.Equals("phonecall"))
            {
                throw new InvalidPluginExecutionException("Entity of wrong type: " + activityName + "!");
            }

            // init tracing service (trace logs only show if an exception occurs?)
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            tracingService.Trace("In plugin - registration validated.");

            // get created activity
            Entity activity = (Entity)context.InputParameters["Target"];
            tracingService.Trace("activity read...");

            // init org service with system account (null) - could use user id from context
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = serviceFactory.CreateOrganizationService(null);

            tracingService.Trace("Retrieving the activity from db..");

            Entity dbActivity = new Entity();
            if (context.OutputParameters.Contains("id"))
            {
                Guid activityId = new Guid(context.OutputParameters["id"].ToString());
                dbActivity = service.Retrieve(activityName, activityId, new ColumnSet(new[] { "regardingobjectid", "statecode" }));

                if (dbActivity == null)
                {
                    throw new InvalidPluginExecutionException("Activity not found!");
                }
                tracingService.Trace("Activity retrieved..");

                int stateCode = dbActivity.GetAttributeValue<OptionSetValue>("statecode").Value;
                if (stateCode == 0)
                {
                    EntityReference regObject = dbActivity.GetAttributeValue<EntityReference>("regardingobjectid");
                    Guid accId = regObject.Id;

                    tracingService.Trace("Acc id retrieved from created activity: " + accId + ", recalculating..");

                    CalculateRollupFieldRequest crfr = new CalculateRollupFieldRequest
                    {
                        Target = new EntityReference("account", accId),
                        FieldName = "new_next_activity"
                    };
                    service.Execute(crfr);

                    tracingService.Trace("Finished..");
                }
                else
                {
                    throw new InvalidPluginExecutionException("Wrong state code: " + stateCode);
                }
            }
        }
    }
}
