﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using System;

namespace AccountActivities
{
    public class ActivityDeletedPlugin: IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // checking plugin registration
            if (context.Stage != 40)
            {
                throw new InvalidPluginExecutionException("Must run as post-op...");
            }
            if (!context.MessageName.Equals("Delete"))
            {
                throw new InvalidPluginExecutionException("Should be registered as Delete not as " + context.MessageName + "!");
            }
            string activityName = context.PrimaryEntityName;
            if (!activityName.Equals("task")
                && !activityName.Equals("appointment")
                && !activityName.Equals("email")
                && !activityName.Equals("phonecall"))
            {
                throw new InvalidPluginExecutionException("Entity of wrong type: " + activityName + "!");
            }

            // init tracing service (trace logs only show if an exception occurs?)
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            tracingService.Trace("In plugin - registration validated.");

            // Obtain the account id from the execution context shared variables.
            IPluginExecutionContext executionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            if (executionContext.SharedVariables.Contains("AccountId"))
            {
                var accId = new Guid((string)executionContext.SharedVariables["AccountId"]);
                tracingService.Trace("Account id read: "+ accId);

                // init org service with system account (null) - could use user id from context
                IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = serviceFactory.CreateOrganizationService(null);

                tracingService.Trace("Recalculating next activity..");
                CalculateRollupFieldRequest crfr = new CalculateRollupFieldRequest
                {
                    Target = new EntityReference("account", accId),
                    FieldName = "new_next_activity"
                };
                service.Execute(crfr);

                tracingService.Trace("Finished..");
            }
        }
    }
}
