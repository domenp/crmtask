﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;

namespace AccountActivities
{
    public class ActivityDeletedPreEventPlugin: IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            var context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            // checking plugin registration
            if (context.Stage != 20)
            {
                throw new InvalidPluginExecutionException("Must run as pre-op...");
            }
            if (!context.MessageName.Equals("Delete"))
            {
                throw new InvalidPluginExecutionException("Should be registered as Delete not as " + context.MessageName + "!");
            }
            string activityName = context.PrimaryEntityName;
            if (!activityName.Equals("task")
                && !activityName.Equals("appointment")
                && !activityName.Equals("email")
                && !activityName.Equals("phonecall"))
            {
                throw new InvalidPluginExecutionException("Entity of wrong type: " + activityName + "!");
            }

            // init tracing service (trace logs only show if an exception occurs?)
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            tracingService.Trace("In pre-plugin - registration validated.");

            // get Deleted activity
            EntityReference activity = (EntityReference)context.InputParameters["Target"];
            tracingService.Trace("activity read...");

            // init org service with system account (null) - could use user id from context
            IOrganizationServiceFactory serviceFactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = serviceFactory.CreateOrganizationService(null);

            tracingService.Trace("Retrieving the activity from db..");
            Entity dbActivity = new Entity();

            Guid activityId = activity.Id;
            dbActivity = service.Retrieve(activityName, activityId, new ColumnSet(new[] { "regardingobjectid" }));

            if (dbActivity == null)
            {
                throw new InvalidPluginExecutionException("Activity not found!");
            }
            tracingService.Trace("Activity retrieved..");

            EntityReference regObject = dbActivity.GetAttributeValue<EntityReference>("regardingobjectid");
            tracingService.Trace("Acc id retrieved from created activity: " + regObject.Id + "storing data..");

            // Pass the data to the post event plug-in in an execution context shared
            // variable named AccountId.
            IPluginExecutionContext executionContext = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            executionContext.SharedVariables.Add("AccountId", (Object)regObject.Id.ToString());

            tracingService.Trace("Finished pre-event..");
        }
    }
}
