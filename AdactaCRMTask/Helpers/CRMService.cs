﻿using AdactaCRMTask.Models;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security;
using System.Threading.Tasks;

namespace AdactaCRMTask.Helpers
{
    public class CRMService
    {
        private const string _serviceUrl = "https://domenp.api.crm4.dynamics.com/";
        private const string _apiUrl = "api/data/v8.2";
        private const string _clientId = "8d876d8d-f953-4632-88bb-37f02eee37ab";
        private const string _redirectUrl = "http://localhost/CRMapp";        
        private static HttpMessageHandler _messageHandler;        

        public CRMService()
        {
            _messageHandler = new OAuthMessageHandler(_serviceUrl, _clientId, _redirectUrl, new HttpClientHandler());
        }

        public async Task<List<AccountDetail>> GetAccounts()
        {
            try
            {
                using (HttpClient httpClient = new HttpClient(_messageHandler))
                {
                    httpClient.BaseAddress = new Uri(_serviceUrl);
                    httpClient.Timeout = new TimeSpan(0, 2, 0);  //2 minutes

                    HttpResponseMessage data = await httpClient.GetAsync(_apiUrl +
                        "/accounts?$select=" + String.Join(",", Constants.AccountProperties.Keys) +
                        "&$expand=contact_customer_accounts($select=" + String.Join(",", Constants.ContactDataProperties) + ")"
                    );

                    if (data.StatusCode == HttpStatusCode.OK)
                    {
                        JObject Retrieveddata = JsonConvert.DeserializeObject<JObject>(await data.Content.ReadAsStringAsync());

                        JToken valArray;
                        List<AccountDetail> accountsListResponse = new List<AccountDetail>();                        
                        if (Retrieveddata.TryGetValue("value", out valArray))
                        {
                            //  process multiple results from "value"
                            foreach (JObject entity in valArray.Children())
                            {
                                AccountDetail account = MapResultsToModel(entity);
                                string getContactsRequest = entity["contact_customer_accounts@odata.nextLink"].ToString();
                                account.AccContactsInfo = await getAccountContacts(httpClient, getContactsRequest);
                                accountsListResponse.Add(account);
                            }
                        }
                        else
                        {
                            // process single result
                            AccountDetail account = MapResultsToModel(Retrieveddata.ToObject<JObject>());
                            string contactsinfoRequest = _apiUrl + "/accounts(" + account.AccountId + ")/contact_customer_accounts?$select="
                                + String.Join(",", Constants.ContactDataProperties);

                            account.AccContactsInfo = await getAccountContacts(httpClient, contactsinfoRequest);
                            accountsListResponse.Add(account);
                        }
                        return accountsListResponse;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private AccountDetail MapResultsToModel(JObject entity)
        {
            AccountDetail account = new AccountDetail();
            foreach (var prop in Constants.AccountProperties)
            {
                if (prop.Value.Equals("NextActivity"))
                {
                    var dateTimeJ = entity[prop.Key];
                    if (dateTimeJ.Type != JTokenType.Null)
                    {
                        DateTime dateTime = dateTimeJ.ToObject<DateTime>();
                        account.NextActivity = dateTime.ToString("d");
                    }
                }
                else
                {
                    account.GetType().GetProperty(prop.Value).SetValue(account, entity[prop.Key] != null ? entity[prop.Key].ToString() : null);
                }
            }
            return account;
        }

        private async Task<AccContactsInfo> getAccountContacts(HttpClient httpClient, string getContactsRequest)
        {
            AccContactsInfo accContactsInfoResponse = new AccContactsInfo();

            HttpResponseMessage data = await httpClient.GetAsync(getContactsRequest);
            if (data.StatusCode == HttpStatusCode.OK)
            {
                JObject Retrieveddata = JsonConvert.DeserializeObject<JObject>(await data.Content.ReadAsStringAsync());

                JToken valArray;
                if (Retrieveddata.TryGetValue("value", out valArray))
                {
                    int countNonEmptyContacts = 0;
                    foreach (JObject entity in valArray.Children())
                    {
                        accContactsInfoResponse.NumberOfContacts += 1;
                        
                        foreach (var prop in Constants.ContactDataProperties)
                        {                            
                            if (!String.IsNullOrEmpty(entity[prop].ToString()))
                            {
                                countNonEmptyContacts++;
                                break;
                            }                            
                        }                        
                    }
                    if (countNonEmptyContacts < accContactsInfoResponse.NumberOfContacts)
                    {
                        accContactsInfoResponse.ContactWithoutData = true;
                    }         
                }
            }
            return accContactsInfoResponse;
        }
    }    

    class OAuthMessageHandler : DelegatingHandler
    {
        private AuthenticationHeaderValue authHeader;

        public OAuthMessageHandler(string serviceUrl, string clientId, string redirectUrl, HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
            AuthenticationContext authContext = new AuthenticationContext("https://login.microsoftonline.com/bb00d6b6-a0b8-4d7c-bb84-f06c1f6ab5a4/oauth2/authorize", false);

            var secure = new SecureString();
            foreach (var character in "MojeGeslo!".ToCharArray())
            {
                secure.AppendChar(character);
            }
            secure.MakeReadOnly();

            UserCredential userCredential = new UserCredential("domen.perenic@domenp.onmicrosoft.com", secure);
            AuthenticationResult authResult = authContext.AcquireToken(serviceUrl, clientId, userCredential);
            authHeader = new AuthenticationHeaderValue("Bearer", authResult.AccessToken);
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            request.Headers.Authorization = authHeader;
            return base.SendAsync(request, cancellationToken);
        }
    }
}