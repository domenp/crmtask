﻿using System.Collections.Generic;

namespace AdactaCRMTask.Models
{
    public class Constants
    {
        public static string[] ContactDataProperties = {
            "address1_composite",
            "address2_composite",
            "address3_composite",
            "business2",
            "callback",
            "emailaddress1",
            "emailaddress2",
            "emailaddress3",
            "fax",
            "home2",
            "mobilephone",
            "telephone1",
            "telephone2",
            "telephone3",
            "websiteurl"
        };

        public static Dictionary<string, string> AccountProperties = new Dictionary<string, string> {
            { "accountid", "AccountId" },
            { "name", "AccountName" },
            {"address1_composite", "AccountAddress" },
            { "new_next_activity", "NextActivity" }
        };
    }
}