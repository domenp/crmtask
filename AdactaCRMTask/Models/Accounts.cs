﻿namespace AdactaCRMTask.Models
{
    public class AccountDetail
    {
        public string AccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountAddress { get; set; }
        public string NextActivity { get; set; }
        public AccContactsInfo AccContactsInfo { get; set; }
    }

    public class AccContactsInfo
    {
        public int NumberOfContacts { get; set; }
        public bool ContactWithoutData { get; set; }
    }
}