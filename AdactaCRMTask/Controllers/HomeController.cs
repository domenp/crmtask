﻿using AdactaCRMTask.Helpers;
using AdactaCRMTask.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace AdactaCRMTask.Controllers
{
    public class HomeController : Controller
    {
        public async System.Threading.Tasks.Task<ActionResult> Index()
        {
            CRMService service = new CRMService();
            List<AccountDetail> getAccountsResponse = await service.GetAccounts();

            return View(getAccountsResponse != null ? getAccountsResponse : new List<AccountDetail>());
        }
    }
}